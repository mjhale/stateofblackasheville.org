class CreatePapers < ActiveRecord::Migration
  def change
    create_table :papers do |t|
      t.string :title
      t.string :attachment
      t.date :year_written

      t.timestamps
    end
  end
end
