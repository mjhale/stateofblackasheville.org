class StudentsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :only_allow_student

  def index
    
  end

  # This method limits access to users with the admin or student role
  def only_allow_student
    redirect_to root_path, :alert => 'Not authorized as a student.' unless current_user.has_any_role? :admin, { :name => :student }
  end

end
