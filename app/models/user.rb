class User < ActiveRecord::Base
	rolify
  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for our model
  #   name added
  attr_accessible :name, :email, :password, :password_confirmation, :remember_me

  # attr_accessible :title, :body

  # Validate presence + uniqueness of "name" attribute
  validates_presence_of :name
  validates_uniqueness_of :name, :email, :case_sensitive => false
end
