class Paper < ActiveRecord::Base
  attr_accessible :attachment, :title, :year_written
end
